terraform {
  required_providers {
    aws = {
      source = "hashicorp/aws"
      version = ">= 3.60.0"
    }
    custom = {
      source = "nazarewk-iac/custom"
      version = "0.3.0"
    }
  }
}
