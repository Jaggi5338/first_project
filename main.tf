// terraform {
//   required_providers {
//     aws = {
//       source = "hashicorp/aws"
//       version = "3.64.2"
//     }
//   }
// }


// #############################################################
// ##S3 Bucket for billing report ##############################
// resource "aws_s3_bucket" "quicksite" {
//   bucket = "quicksitetestreport"
//   versioning {
//     enabled = true
//   }
  
// }

// ##############################################################
// ## S3 bucket policy###########################################

// resource "aws_s3_bucket_policy" "b" {
//   bucket = aws_s3_bucket.quicksite.id

//   # Terraform's "jsonencode" function converts a
//   # Terraform expression's result to valid JSON syntax.
//   policy = jsonencode({
    
//   "Version": "2008-10-17",
//   "Id": "Policy1335892530063",
//   "Statement": [
//     {
//       "Sid": "Stmt1335892150622",
//       "Effect": "Allow",
//       "Principal": {
//         "Service": "billingreports.amazonaws.com"
//       },
//       "Action": [
//         "s3:GetBucketAcl",
//         "s3:GetBucketPolicy"
//       ],
//       "Resource": "arn:aws:s3:::quicksitetestreport"
//     },
//     {
//       "Sid": "Stmt1335892526596",
//       "Effect": "Allow",
//       "Principal": {
//         "Service": "billingreports.amazonaws.com"
//       },
//       "Action": [
//         "s3:PutObject"
//       ],
//       "Resource": "arn:aws:s3:::quicksitetestreport/*"
//     }
//   ]

//   })
// }

// ##############################################################
// ## Enable Cost & usage report#################################

// ##############################################################
// ##Lambda execution role ######################################

// resource "aws_iam_role" "lambda_execution" {
//   name = "testing-AWSS3CURLambdaExecutor"

//   assume_role_policy = <<EOF
// {
//   "Version": "2012-10-17",
//   "Statement": [

//     {
//       "Action": "sts:AssumeRole",
//       "Principal": {
//         "Service": "lambda.amazonaws.com"
//       },
//       "Effect": "Allow",
//       "Sid": ""
//     }
//   ]
// }
// EOF
// }

// resource "aws_iam_policy" "quick01" {
//   name        = "lambda_executor_policy"
//   description = "A test policy"

//   policy = <<EOF
// {
//    "Version": "2012-10-17",
//     "Statement": [
//         {
//             "Action": [
//                 "logs:CreateLogGroup",
//                 "logs:CreateLogStream",
//                 "logs:PutLogEvents"
//             ],
//             "Resource": "arn:aws:logs:*:*:*",
//             "Effect": "Allow"
//         },
//         {
//             "Action": [
//                 "s3:PutBucketNotification"
//             ],
//             "Resource": "arn:aws:s3:::quicksitereport",
//             "Effect": "Allow"
//         }
//     ]
// }
// EOF
// }

// resource "aws_iam_role_policy_attachment" "test-attach" {
//   role       = aws_iam_role.lambda_execution.name

//   policy_arn = aws_iam_policy.quick01.arn
// }

// ##################################################################
// ##A recurring crawler that keeps your CUR table in Athena up-to-date####################################################

// resource "aws_glue_crawler" "crawler" {
//   database_name = aws_glue_catalog_database.quicksitedb.name
//   name          = "crawler"
//   role          = aws_iam_role.lambda_execution2.arn
//   description   = "A recurring crawler that keeps your CUR table in Athena up-to-date."
  


//   s3_target {
//     path = "s3://${aws_s3_bucket.quicksite.bucket}"
    
//   }
//     SchemaChangePolicy {
//           UpdateBehavior = "UPDATE_IN_DATABASE"
//           DeleteBehavior = "DELETE_FROM_DATABASE"
//     }
// }

// ##############################################################
// ##Lambda execution role 2 ######################################

// resource "aws_iam_role" "lambda_execution2" {
//   name = "testing-AWSS3CURLambdaExecutor2"

//    assume_role_policy = <<EOF
// {
//   "Version": "2012-10-17",
//   "Statement": [
//     {
//       "Action": "sts:AssumeRole",
//       "Principal": {
//         "Service": "glue.amazonaws.com"
//       },
//       "Effect": "Allow",
//       "Sid": ""
//     }
//   ]
// }
// EOF
// }
// resource "aws_iam_policy" "quick02" {
//   name        = "lambda_executor2_policy"
//   description = "A test policy"

//   policy = <<EOF
// {
//    "Version": "2012-10-17",
//     "Statement": [
//         {
//             "Action": [
//                 "logs:CreateLogGroup",
//                 "logs:CreateLogStream",
//                 "logs:PutLogEvents"
//             ],
//             "Resource": "arn:aws:logs:*:*:*",
//             "Effect": "Allow"
//         },
//         {
//             "Action": [
//                 "s3:PutBucketNotification"
//             ],
//             "Resource": "arn:aws:s3:::quicksitereport",
//             "Effect": "Allow"
//         }
//     ]
// }
// EOF
// }

// resource "aws_iam_policy" "quick03" {
//   name        = "lambda_executor3_policy"
//   description = "A test policy"

//   policy = <<EOF
// {
//    "Version": "2012-10-17",
//     "Statement": [
//         {
//             "Action": [
//                 "logs:CreateLogGroup",
//                 "logs:CreateLogStream",
//                 "logs:PutLogEvents"
//             ],
//             "Resource": "arn:aws:logs:*:*:*",
//             "Effect": "Allow"
//         },
//         {
//             "Action": [
//                 "s3:PutBucketNotification"
//             ],
//             "Resource": "arn:aws:s3:::quicksitereport",
//             "Effect": "Allow"
//         }
//     ]
// }
// EOF
// }

// resource "aws_iam_role_policy_attachment" "test-attach02" {
//   role       = aws_iam_role.lambda_execution2.name
//   policy_arn = aws_iam_policy.quick02.arn
// }

// #######################################################################
// ##AWS Glue Database for saving reports in table#####################################################
// resource "aws_glue_catalog_database" "quicksitedb" {
//   name = "quicksitedb"
//   description = "AWS Glue Database for saving reports in table"
// }

// #######################################################################
// ##aws_glue_crawler#####################################################
