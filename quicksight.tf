resource "aws_glue_catalog_database" "aws_glue_catalog_database" {
  name = "athenacurcfn_quicksight_vivek"
}

resource "aws_iam_role" "glue_role" {
  name = "glue_role"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "glue.amazonaws.com"
        }
      },
    ]
  })
}


resource "aws_iam_role_policy" "glue_role_policy" {
  name = "glue_role_policy"
  role = aws_iam_role.glue_role.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:PutObject",
                "s3:GetObject",
                "glue:ImportCatalogToGlue",
                "logs:CreateLogStream",
                "glue:UpdateDatabase",
                "glue:CreateTable",
                "glue:UpdateTable",
                "logs:CreateLogGroup",
                "glue:UpdatePartition"
            ],
            "Resource": [
                "arn:aws:s3:::demo000111/report/demo/demo*",
                "arn:aws:logs:*:643628309734:log-group:*",
                "arn:aws:glue:*:643628309734:database/*",
                "arn:aws:glue:*:643628309734:catalog",
                "arn:aws:glue:*:643628309734:table/*/*"
            ]
        },
        {
            "Effect": "Allow",
            "Action": "*",
            "Resource": "*"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "logs:PutLogEvents",
            "Resource": "arn:aws:logs:*:643628309734:log-group:*:log-stream:*"
        }
      ]

  })
}

resource "aws_iam_role" "AWSCURCrawlerLambdaExecutor" {
  name = "AWSCURCrawlerLambdaExecutor"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy" "AWSCURCrawlerLambdaExecutor_policy" {
  name = "AWSCURCrawlerLambdaExecutor_policy"
  role = aws_iam_role.AWSCURCrawlerLambdaExecutor.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": "glue:StartCrawler",
            "Resource": "*"
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": "logs:PutLogEvents",
            "Resource": "arn:aws:logs:*:643628309734:log-group:*:log-stream:*"
        },
        {
            "Sid": "VisualEditor2",
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:CreateLogGroup"
            ],
            "Resource": "arn:aws:logs:*:643628309734:log-group:*"
        },
        {
            "Sid": "VisualEditor3",
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
  })
}

resource "aws_glue_crawler" "example" {
  database_name = aws_glue_catalog_database.aws_glue_catalog_database.name
  name          = "AWSCURCrawler-quicksight_vivek"
  role          = aws_iam_role.glue_role.arn
  #Description   = "A recurring crawler that keeps your CUR table in Athena up-to-date"
  schema_change_policy {
    update_behavior = "UPDATE_IN_DATABASE"
    delete_behavior = "DELETE_FROM_DATABASE"
  }

  s3_target {
    path = "s3://demo000111"
  }
}

#Link : https://gist.github.com/smithclay/e026b10980214cbe95600b82f67b4958

// data "archive_file" "lambda_zip" {
//     type          = "zip"
//     source_file   = "index.js"
//     output_path   = "lambda_function_payload.zip"
// }

// resource "aws_lambda_function" "AWSCURInitializer" {
//   filename                       = "lambda_function_payload.zip"
//   function_name                  = "AWSCURInitializer"
//   role                           = aws_iam_role.AWSCURCrawlerLambdaExecutor.arn
//   handler                        = "index.handler"
//   timeout                        = 30
//   runtime                        = "nodejs12.x"
//   reserved_concurrent_executions = 1

//   # The filebase64sha256() function is available in Terraform 0.11.12 and later
//   # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
//   # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
//   source_code_hash = "${data.archive_file.lambda_zip.output_base64sha256}"

// }

resource "aws_lambda_function" "AWSCURInitializer" {
  function_name                  = "AWSCURInitializer"
  s3_bucket                      = "demo000111-lambda"
  s3_key                         = "lambda/AWSCURInitializer.zip"
  role                           = aws_iam_role.AWSCURCrawlerLambdaExecutor.arn
  handler                        = "index.handler"
  timeout                        = 30
  runtime                        = "nodejs12.x"
  reserved_concurrent_executions = 0

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  #source_code_hash = "${data.archive_file.lambda_zip_s3.output_base64sha256}"
}


resource "aws_glue_job" "AWSStartCURCrawler" {
  name     = "AWSStartCURCrawler"
  role_arn = aws_iam_role.glue_role.arn
  command {
    script_location = aws_lambda_function.AWSCURInitializer.arn
  }
}

resource "aws_glue_trigger" "AWSStartCURCrawler_trigger" {
  name = "AWSStartCURCrawler_trigger"
  type = "ON_DEMAND"

  actions {
    job_name = aws_glue_job.AWSStartCURCrawler.name
  }
}

resource "aws_lambda_permission" "allow_AWSCURInitializer" {
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.AWSCURInitializer.function_name
  principal     = "s3.amazonaws.com"
  source_arn    = "arn:aws:s3:::demo000111"
}

resource "aws_iam_role" "AWSS3CURLambdaExecutor" {
  name = "AWSS3CURLambdaExecutor"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  assume_role_policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = "sts:AssumeRole"
        Effect = "Allow"
        Sid    = ""
        Principal = {
          Service = "lambda.amazonaws.com"
        }
      },
    ]
  })
}

resource "aws_iam_role_policy" "AWSS3CURLambdaExecutor_policy" {
  name = "AWSCURCrawlerLambdaExecutor_policy"
  role = aws_iam_role.AWSS3CURLambdaExecutor.id

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    "Version": "2012-10-17",
    "Statement": [
        {
            "Sid": "VisualEditor0",
            "Effect": "Allow",
            "Action": [
                "s3:PutBucketNotification",
                "logs:PutLogEvents"
            ],
            "Resource": [
                "arn:aws:logs:*:643628309734:log-group:*:log-stream:*",
                "arn:aws:s3:::demo000111/*"
            ]
        },
        {
            "Sid": "VisualEditor1",
            "Effect": "Allow",
            "Action": [
                "logs:CreateLogStream",
                "logs:CreateLogGroup"
            ],
            "Resource": "arn:aws:logs:*:643628309734:log-group:*"
        },
        {
            "Sid": "VisualEditor2",
            "Effect": "Allow",
            "Action": "s3:*",
            "Resource": "*"
        }
    ]
  })
}

// data "archive_file" "lambda_zip_s3" {
//     type          = "zip"
//     source_file   = "s3index.js"
//     output_path   = "lambda_function_payload_s3.zip"
// }

// resource "aws_lambda_function" "AWSS3CURNotification" {
//   filename                       = "lambda_function_payload_s3.zip"
//   function_name                  = "AWSS3CURNotification"
//   role                           = aws_iam_role.AWSS3CURLambdaExecutor.arn
//   handler                        = "index.handler"
//   timeout                        = 30
//   runtime                        = "nodejs12.x"
//   reserved_concurrent_executions = 1

//   # The filebase64sha256() function is available in Terraform 0.11.12 and later
//   # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
//   # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
//   source_code_hash = "${data.archive_file.lambda_zip_s3.output_base64sha256}"
// }

resource "custom_resource" "AWSStartCURCrawler" {
  ServiceToken =  aws_lambda_function.AWSS3CURNotification.arn
  TargetLambdaArn = aws_lambda_function.AWSCURInitializer.arn
  BucketName = demo000111
  ReportKey = report/demo/demo
}

resource "aws_lambda_function" "AWSS3CURNotification" {
  function_name                  = "AWSS3CURNotification"
  s3_bucket                      = "demo000111-lambda"
  s3_key                         = "lambda/AWSS3CURNotification.zip"
  role                           = aws_iam_role.AWSS3CURLambdaExecutor.arn
  handler                        = "index.handler"
  timeout                        = 30
  runtime                        = "nodejs12.x"
  reserved_concurrent_executions = 0

  # The filebase64sha256() function is available in Terraform 0.11.12 and later
  # For Terraform 0.11.11 and earlier, use the base64sha256() function and the file() function:
  # source_code_hash = "${base64sha256(file("lambda_function_payload.zip"))}"
  #source_code_hash = "${data.archive_file.lambda_zip_s3.output_base64sha256}"
}

resource "aws_glue_catalog_table" "aws_glue_catalog_table" {
  name          = "awscurreportstatustable"
  database_name = aws_glue_catalog_database.aws_glue_catalog_database.name

  table_type = "EXTERNAL_TABLE"

  parameters = {
    EXTERNAL              = "TRUE"
    "parquet.compression" = "SNAPPY"
  }

  storage_descriptor {
    location      = "demo000111/report/demo/demo/cost_and_usage_data_status/"
    input_format  = "org.apache.hadoop.hive.ql.io.parquet.MapredParquetInputFormat"
    output_format = "org.apache.hadoop.hive.ql.io.parquet.MapredParquetOutputFormat"

    ser_de_info {
      name                  = "my-stream"
      serialization_library = "org.apache.hadoop.hive.ql.io.parquet.serde.ParquetHiveSerDe"

      parameters = {
        "serialization.format" = 1
      }
    }

    columns {
      name = "status"
      type = "string"
    }

  }
}

#Link : https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/s3_bucket_notification

resource "aws_s3_bucket_notification" "AWSPutS3CURNotification" {
  bucket = "demo000111"

  lambda_function {
    lambda_function_arn = aws_lambda_function.AWSS3CURNotification.arn
    events              = ["s3:ObjectCreated:*"]
    filter_prefix       = "AWSLogs/"
    filter_suffix       = ".log"
  }
  // depends_on = [aws_lambda_permission.data.aws_s3_bucket.s3-alpha-d-ew1-quicksight-vivek.id]
}


